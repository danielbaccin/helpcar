package br.com.helpCar.activity;

import br.com.helpCar.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity{
	
	private Spinner marcas;
	private Spinner modelos;
	private Spinner versoes;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		ArrayAdapter<CharSequence> adapterMarcas = ArrayAdapter.createFromResource(this, R.array.marcas, android.R.layout.simple_spinner_item);
		marcas = (Spinner) findViewById(R.id.marca);
		marcas.setAdapter(adapterMarcas);

		ArrayAdapter<CharSequence> adapterModelos = ArrayAdapter.createFromResource(this, R.array.modelos, android.R.layout.simple_spinner_item);
		modelos = (Spinner) findViewById(R.id.modelo);
		modelos.setAdapter(adapterModelos);

		ArrayAdapter<CharSequence> adapterVersoes = ArrayAdapter.createFromResource(this, R.array.versoes, android.R.layout.simple_spinner_item);
		versoes = (Spinner) findViewById(R.id.versao);
		versoes.setAdapter(adapterVersoes);
	}
	

}
