package br.com.helpCar.activity;

import br.com.helpCar.R;
import br.com.helpCar.model.Constantes;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OleoActivity extends Activity{
	
	private TextView carroSelecionado;
	private TextView referenciaDoOloe;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.oleo);
		
		referenciaDoOloe = (TextView) findViewById(R.id.descricao_referencia_oleo);
		referenciaDoOloe.setText(" MI60AD");
		
		String nomeDoVeiculo = getIntent().getExtras().getString(Constantes.NOME_VEICULO);
		carroSelecionado =  (TextView) findViewById(R.id.carro_selecionado);
		carroSelecionado.setText(nomeDoVeiculo);
	}
	
	
	public void irParaListaDeEstabelecimentos(View view){
		Intent intent = new Intent(this, ListaDeEstabelecimentosActivity.class);
		startActivity(intent);
	}
	
	
	
	

}
