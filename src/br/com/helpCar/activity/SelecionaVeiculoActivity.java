package br.com.helpCar.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import br.com.helpCar.R;
import br.com.helpCar.model.Constantes;

public class SelecionaVeiculoActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seleciona_veiculo);
		
		AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.nome_veiculo_selecionado);
		String[] carros = getResources().getStringArray(R.array.versoes);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, carros);
		textView.setAdapter(adapter);
	}
	
	
	public void aplicaFiltro(View view) {
		Intent intent = new Intent(this, DashBoardActivity.class);
		TextView nomeDoVeiculo = (TextView) findViewById(R.id.nome_veiculo_selecionado);
		intent.putExtra(Constantes.NOME_VEICULO, nomeDoVeiculo.getText().toString());
		startActivity(intent);
	}

}
