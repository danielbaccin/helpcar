package br.com.helpCar.activity;

import br.com.helpCar.R;
import br.com.helpCar.model.Constantes;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PneuActivity extends Activity{
	
	private TextView carroSelecionado;
	private TextView descricaoDoModeloOriginal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pneu);
		
		descricaoDoModeloOriginal = (TextView) findViewById(R.id.descricao_modelo_original);
		descricaoDoModeloOriginal.setText(" 195/55 R16");
		
		String nomeDoVeiculo = getIntent().getExtras().getString(Constantes.NOME_VEICULO);
		carroSelecionado =  (TextView) findViewById(R.id.carro_selecionado);
		carroSelecionado.setText(nomeDoVeiculo);
		
	}

	public void irParaListaDeEstabelecimentos(View view){
		Intent intent = new Intent(this, ListaDeEstabelecimentosActivity.class);
		startActivity(intent);
	}
}

