package br.com.helpCar.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import br.com.helpCar.R;
import br.com.helpCar.model.Constantes;

public class DashBoardActivity extends Activity{
	
	private TextView carroSelecionado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		
		String nomeDoVeiculo = getIntent().getExtras().getString(Constantes.NOME_VEICULO);
		carroSelecionado = (TextView) findViewById(R.id.nome_do_carro);
		carroSelecionado.setText(nomeDoVeiculo);
	}
	
	public void selecionarOpcao(View view) {
		TextView nomeDoVeiculo = (TextView) findViewById(R.id.nome_do_carro);
		switch (view.getId()) {
			case R.id.consultar_pneus:
				Intent irParaPneus = new Intent(this, PneuActivity.class);
				irParaPneus.putExtra(Constantes.NOME_VEICULO, nomeDoVeiculo.getText().toString());
				startActivity(irParaPneus);
				break;
			case R.id.consultar_baterias:
				Intent irParaBateria = new Intent(this, BateriaActivity.class);
				irParaBateria.putExtra(Constantes.NOME_VEICULO, nomeDoVeiculo.getText().toString());
				startActivity(irParaBateria);
				break;
			case R.id.consultar_oleo:
				Intent irParaOleo = new Intent(this, BateriaActivity.class);
				irParaOleo.putExtra(Constantes.NOME_VEICULO, nomeDoVeiculo.getText().toString());
				startActivity(irParaOleo);
				break;
			case R.id.consultar_reboque:
				Intent irParaListaDeReboques = new Intent(this, ListaDeEstabelecimentosActivity.class);
				irParaListaDeReboques.putExtra(Constantes.LISTA_REBOQUES, "lista");
				startActivity(irParaListaDeReboques);

		}
	}

	public void irParaSelecionarVeiculo(View view) {
		Intent intent = new Intent(this, SelecionaVeiculoActivity.class);
		startActivity(intent);
	}

}
