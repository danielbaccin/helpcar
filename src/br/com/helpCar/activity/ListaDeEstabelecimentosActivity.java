package br.com.helpCar.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import br.com.helpCar.R;
import br.com.helpCar.model.Constantes;
import br.com.helpCar.model.Estabelecimento;

public class ListaDeEstabelecimentosActivity extends ListActivity implements OnItemClickListener, OnClickListener, ViewBinder{
	
	private List<Map<String, Object>> estabelecimentos;
	private AlertDialog alertDialog;
	private int estabelecimentoSelecionado;
	private boolean isListaReboques = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getListView().setOnItemClickListener(this);
		
		this.alertDialog = criaAlertDialog();
		new Task().execute();

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemClicado = item.getItemId();
		switch (itemClicado) {
		case 100:
			startActivity(new Intent(this, DashBoardActivity.class));
			return false;
		default:
            return super.onOptionsItemSelected(item);
		}
		
	}
	
	private AlertDialog criaAlertDialog() {
		final CharSequence[] items = {
				getString(R.string.ligar),
				getString(R.string.ver_no_mapa) 
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.opcoes);
			builder.setItems(items, this);
			return builder.create();
	}

	private List<Map<String, Object>>  listarEstabelecimentos() {
		List<Estabelecimento> listaEstaticaDeEstabelecimentos = isListaReboques ? criaUmListaEstaticaDeReboques() :  criaUmListaEstaticaDeEstabelecimentos();
		estabelecimentos = new ArrayList<Map<String, Object>>();
		Map<String, Object> item = null;
		for (Estabelecimento estabelecimento : listaEstaticaDeEstabelecimentos) {
			item = new HashMap<String, Object>();
			item.put("id", estabelecimento.getId());
			if(estabelecimento.getId().compareTo(Long.valueOf(1)) == 0){
				item.put("logo", R.drawable.staron);
			}else{
				item.put("logo", R.drawable.staroff);
			}
			item.put("nome", estabelecimento.getNome());
			item.put("endereco", estabelecimento.getEndereco());
			item.put("telefone", estabelecimento.getTelefone());
			estabelecimentos.add(item);
		}
		return estabelecimentos;
	}
	
	
	private List<Estabelecimento> criaUmListaEstaticaDeReboques() {
		List<Estabelecimento> lista = new ArrayList<Estabelecimento>();
		
		Estabelecimento reboque_1 = criaEstabelecimento(1, "REboque 1", "av aguanabi 100", "32951033");
		lista.add(reboque_1);

		Estabelecimento reboque_2 = criaEstabelecimento(1, "REboque 2", "av oliveira paiva 50", "40113000");
		lista.add(reboque_2);
		
		Estabelecimento reboque_3 = criaEstabelecimento(1, "REboque 3", "av washinton soares 1025", "40113000");
		lista.add(reboque_3);
		
		return lista;
	}


	private List<Estabelecimento> criaUmListaEstaticaDeEstabelecimentos() {
		List<Estabelecimento> lista = new ArrayList<Estabelecimento>();
		
		Estabelecimento estabelecimento_1 = criaEstabelecimento(1, "Sam Pneus", "av miguel dias 100", "32951033");
		lista.add(estabelecimento_1);

		Estabelecimento estabelecimento_2 = criaEstabelecimento(1, "Teste Pneus", "av dede brasil 3850", "88121128");
		lista.add(estabelecimento_2);
		
		return lista;
	}

	private Estabelecimento criaEstabelecimento(int i, String nome, String endereco, String telefone) {
		Estabelecimento estabelecimento = new Estabelecimento();
		estabelecimento.setId(Long.valueOf(i));
		estabelecimento.setNome(nome);
		estabelecimento.setEndereco(endereco);
		estabelecimento.setTelefone(telefone);
		return estabelecimento;
	}


	private class Task extends AsyncTask<Void, Void, List<Map<String, Object>>>{
		@Override
		protected List<Map<String, Object>> doInBackground(Void... params) {
			return listarEstabelecimentos();
		}
		@Override
		protected void onPostExecute(List<Map<String, Object>> result) {
			String[] de = {"logo", "nome", "endereco","telefone"};
	
			int[] para = {R.id.logo_estabelecimento, R.id.nome_estabelecimento, 
				   R.id.endereco_estabelecimento, R.id.telefone_estabelecimento};
	
			SimpleAdapter adapter = new SimpleAdapter(ListaDeEstabelecimentosActivity.this, result, R.layout.lista_de_estabelecimentos, de, para);
			adapter.setViewBinder(ListaDeEstabelecimentosActivity.this);
			setListAdapter(adapter);
		}
	}


	@Override
	public boolean setViewValue(View view, Object data,
			String textRepresentation) {
		return false;
	}

	@Override
	public void onClick(DialogInterface dialog, int item) {
		Intent intent;
		Long id = (Long) estabelecimentos.get(estabelecimentoSelecionado).get("id");
		String nomeDoEstabelecimento = (String) estabelecimentos.get(estabelecimentoSelecionado).get("nome");

		switch (item) {
			case 0:
				Intent irParaTelefone = new Intent(Intent.ACTION_CALL);
				String telefone = (String) estabelecimentos.get(estabelecimentoSelecionado).get("telefone");
				Uri numeroDoTelefone =  Uri.parse("tel:"+telefone);
				irParaTelefone.setData(numeroDoTelefone);
				startActivity(irParaTelefone);
				break;

			case 1:
				Intent irParaMapa = new Intent(Intent.ACTION_VIEW);
				String endereco = (String) estabelecimentos.get(estabelecimentoSelecionado).get("endereco");
				Uri localizacao = Uri.parse("geo:0,0?z=14&q="+endereco);
				irParaMapa.setData(localizacao);
				startActivity(irParaMapa);
				break;
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
		estabelecimentoSelecionado = position;
		alertDialog.show();
	}

}
